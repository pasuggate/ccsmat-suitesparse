------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.SuiteSparse.CSparse
-- Copyright   : (C) Patrick Suggate 2015
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- The low-level bindings to the Consise Sparse (CSParse) matrix library, of
-- the SuiteSparse library.
-- 
-- Changelog:
--  + 08/09/2015  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.CCSMat.SuiteSparse.CSparse
  ( cs2ccs
  , cs2ccsD
  , cs2ccsWith
  , ccs2cs
  , ccs2csD
  , ccs2csWith
  ) where

import Data.Vector.Helpers
import Data.Vector.Storable (Storable)
import qualified Data.Vector.Generic as G

import Data.Matrix.CSparse
import Data.CCSMat.Internal


-- * Conversions between formats.
------------------------------------------------------------------------------
cs2ccs :: (Storable a, RealFrac a) => CSparse -> CCSMat a
{-# INLINE cs2ccs #-}
cs2ccs  = cs2ccsWith realToFrac

cs2ccsD :: CSparse -> CCSD
{-# INLINE [1] cs2ccsD #-}
cs2ccsD  = cs2ccsWith id

cs2ccsWith :: Storable a => (Double -> a) -> CSparse -> CCSMat a
cs2ccsWith f (CSparse _ m n cs rs xs mnz) =
  case mnz of
    Nothing -> let nz = lst cs
                   ys = G.map f $ tak nz xs
               in  CCS m (tak nz rs) cs ys
    Just nz -> let cp = packCols n $ tak nz cs
                   ys = G.map f $ tak nz xs
               in  CCS m (tak nz rs) cp ys

ccs2cs :: (Storable a, RealFrac a) => CCSMat a -> CSparse
{-# INLINE ccs2cs #-}
ccs2cs  = ccs2csWith realToFrac

ccs2csD :: CCSD -> CSparse
{-# INLINE [1] ccs2csD #-}
ccs2csD  = ccs2csWith id

ccs2csWith :: Storable a => (a -> Double) -> CCSMat a -> CSparse
{-# INLINE ccs2csWith #-}
ccs2csWith f (CCS rn ri cp xs) = CSparse nz rn cn cp ri ys Nothing
  where nz = len ri
        cn = len cp - 1
        ys = G.map f xs
