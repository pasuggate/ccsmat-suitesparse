------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.SuiteSparse.CholMod
-- Copyright   : (C) Patrick Suggate 2015
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- The low-level bindings to the CHOLMOD library for computing Cholesky
-- factorisations of symmetric, positive-definite matrices.
-- 
-- Changelog:
--  + 09/09/2015  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.CCSMat.SuiteSparse.CholMod
  ( ccs2cmsD
  ) where

import Control.Monad.IO.Class
import Data.Vector.Helpers
import Data.Matrix.CholMod
import Data.CCSMat.Internal


ccs2cmsD :: MonadIO m => CCSMat Double -> CM m CMSparse
ccs2cmsD (CCS rn ri cp xs) = do
  let (cn, nz) = (len cp - 1, len ri)
--   cs <- allocSparse rn cn nz False False cholmod_s_unsym cholmod_real
  cs <- allocSparse rn cn nz False True cholmod_s_unsym cholmod_real
  setSparseRows ri cs
  setSparseCols cp cs
--   setSparseCnts cp cs
  setSparseVals xs cs
  setSparseDType cholmod_double cs
  setSparseSorted False cs
  return cs

{-- }
ccs2cmsD :: MonadIO m => CCSMat Double -> CM m CMSparse
ccs2cmsD (CCS rn ri cp xs) = do
  let cn = len cp - 1
      nz = len ri
  cs <- allocSparse rn cn nz False True cholmod_s_unsym cholmod_real
  setSparseRows ri cs
  setSparseCols cp cs
  setSparseVals xs cs
  return cs
--}
