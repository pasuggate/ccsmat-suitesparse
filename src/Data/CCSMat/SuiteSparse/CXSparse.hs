------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.SuiteSparse.CXSparse
-- Copyright   : (C) Patrick Suggate 2015
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- The low-level bindings to the Consise eXtended Sparse (CXSParse) matrix
-- library, of the SuiteSparse numerical libraries.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.CCSMat.SuiteSparse.CXSparse
  ( cs2ccs
  , cs2ccsD
  , cs2ccsWith
  , ccs2cs
  , ccs2csD
  , ccs2csWith
  ) where

import Data.Vector.Helpers
import Data.Vector.Storable (Storable)
import qualified Data.Vector.Generic as G

import Data.Matrix.CXSparse
import Data.CCSMat.Internal


-- * Conversions between formats.
------------------------------------------------------------------------------
cs2ccs :: (Storable a, RealFrac a) => CXSparse -> CCSMat a
{-# INLINE cs2ccs #-}
cs2ccs  = cs2ccsWith realToFrac

cs2ccsD :: CXSparse -> CCSD
{-# INLINE [1] cs2ccsD #-}
cs2ccsD  = cs2ccsWith id

cs2ccsWith :: Storable a => (Double -> a) -> CXSparse -> CCSMat a
cs2ccsWith f (CXSparse _ m n cs rs xs mnz) =
  case mnz of
    Nothing -> let nz = lst cs
                   ys = G.map f $ tak nz xs
               in  CCS m (tak nz rs) cs ys
    Just nz -> let cp = packCols n $ tak nz cs
--     Just nz -> let cp = unsafePackCols n $ tak nz cs
                   ys = G.map f $ tak nz xs
               in  CCS m (tak nz rs) cp ys

ccs2cs :: (Storable a, RealFrac a) => CCSMat a -> CXSparse
{-# INLINE ccs2cs #-}
ccs2cs  = ccs2csWith realToFrac

ccs2csD :: CCSD -> CXSparse
{-# INLINE [1] ccs2csD #-}
ccs2csD  = ccs2csWith id

ccs2csWith :: Storable a => (a -> Double) -> CCSMat a -> CXSparse
{-# INLINE ccs2csWith #-}
ccs2csWith f (CCS rn ri cp xs) = CXSparse nz rn cn cp ri ys Nothing
  where nz = len ri
        cn = len cp - 1
        ys = G.map f xs
