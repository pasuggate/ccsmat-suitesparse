module Main where

import Test.Framework (defaultMain, testGroup)
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.QuickCheck


-- * Test-groups for algebraic, sparse-matrix operations.
------------------------------------------------------------------------------
algTests =
  [ testGroup "algebra" algSimple
  ]

algSimple =
  [ testProperty "matrix add          " prop_matrix_add
  , testProperty "matrix multiply     " prop_matrix_multiply
  ]


main =
  defaultMain 
  [ testGroup "CCSMat.SuiteSparse.Algebra" algTests
  ]
