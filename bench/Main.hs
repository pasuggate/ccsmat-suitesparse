{-# LANGUAGE BangPatterns #-}
module Main where

import Foreign
import Foreign.C.Types
import Data.List (scanl')
import Control.Monad.ST
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Generic.Mutable as M
import Criterion.Main (defaultMain, bench, bgroup, whnf)

import Data.CCSMat.Internal
import Data.CCSMat.Vector
import Data.CCSMat.Transpose
import Data.CCSMat.Sort
import Data.CCSMat.Multiply
import qualified Data.CCSMat.OldMult as Old
import Data.CCSMat.Addsub
import qualified Data.CCSMat.File as CCS
import qualified Data.CCSMat.Pipes as CCS
import qualified Data.CCSMat.Shape as CCS
import qualified Data.CCSMat.Stream as CCS
import qualified Data.CCSMat.Bundle as CCS
import Data.Vector.Sparse

import Data.Matrix.CSparse
import Data.CCSMat.SuiteSparse.CSparse


i2p' cn ci = i2p ci (-1) 0
matsum (CCS _ _ _ xs) = Vec.sum xs
matsuM (CSparse _ _ _ _ _ xs _) = Vec.sum xs
-- matsuR (CSparseRaw _ _ _ _ _ xs _) = Vec.sum xs
vecsum (SparseVector _ _ xs) = Vec.sum xs


matbench mm =
  let mM = ccs2csD mm
  in  bgroup "Matrix reshaping"
      [ bench "unsafeTranspose    " $ whnf (matsum . unsafeTranspose   ) mm
--       , bench "matsum             " $ whnf (matsum                     ) mm
      , bench "cs'transpose       " $ whnf (matsuM . flip cs'transpose True) mM
--       , bench "cs'copy            " $ whnf (matsuM . cs'copy           ) mM
      , bench "transpose          " $ whnf (matsum . transpose         ) mm
--       , bench "CCS.tranS          " $ whnf (matsum . CCS.tranS         ) mm
      , bench "Shape.tril         " $ whnf (matsum . CCS.tril          ) mm
--       , bench "CCS.trilS          " $ whnf (matsum . CCS.trilS         ) mm
      , bench "CCS.trilF          " $ whnf (matsum . CCS.trilF         ) mm
      ]

mulbench aa bb =
  let a' = ccs2csD aa
      b' = ccs2csD bb
  in  bgroup "Multiplication"
      [ bench "cs'multiply        " $ whnf (matsuM . cs'multiply     a') b'
      , bench "cs'multiply (2)    " $ whnf (matsuM . cs'multiply     b') a'
      , bench "multiply           " $ whnf (matsum . multiply        aa) bb
      , bench "multiply (2)       " $ whnf (matsum . multiply        bb) aa
--       , bench "multiply   (sorted)" $ whnf (matsum . sort . multiply aa) bb
--       , bench "mulSpMV            " $ whnf (matsum . mulSpMV         aa) bb
--       , bench "mulSpMV (2)        " $ whnf (matsum . mulSpMV         bb) aa
      , bench "fmul               " $ whnf (matsum . fmul            aa) bb
      , bench "fmul (2)           " $ whnf (matsum . fmul            bb) aa
      ]

addbench aa bb =
  let aA = ccs2csD aa
      bB = ccs2csD bb
  in  bgroup "Addition/subtraction"
  [ bench "cs'add             " $ whnf (matsuM . cs'add     aA bB 1) 1
  , bench "add                " $ whnf (matsum . add             aa) bb
  , bench "sub                " $ whnf (matsum . sub             aa) bb
  ]


main = do
  -- Some testing files:
  ee <- CCS.fromFile "/home/patrick/Dropbox/phd/data/eit625_A.mtx"
  xx <- CCS.fromFile "/home/patrick/Dropbox/phd/data/eit625_W1.mtx"
  yy <- CCS.fromFile "/home/patrick/Dropbox/phd/data/eit625_W2.mtx"
  let cn = 5
      zz = xx `multiply` transpose yy
      -- Indices that are {unsorted, sorted, missing-a-column}:
      ci = Vec.fromList [0,4,1,2,2,3,1,0,3,1,4,1,0,4,4,1] :: IVec
      cj = Vec.fromList [0,0,0,1,1,1,1,1,2,2,3,3,4,4,4,4] :: IVec
      ck = Vec.fromList [0,0,0,1,1,1,1,1,3,3,3,3,4,4,4,4] :: IVec
      -- Assemble a CCS sparse matrix:
      ri = Vec.fromList [0,1,3,0,1,2,3,4,1,2,3,4,0,1,3,4] :: IVec
      cp = packCols cn ci :: IVec
      xs = Vec.fromList [1..16] :: DVec
      mm = CCS cn ri cp xs :: CCSD
      -- Assemble another sparse matrix:
      rj = Vec.fromList [3,0,2,4,1,0,3,2,3,1,2,4,2,1,3,4] :: IVec
      cq = packCols cn ck :: IVec
      ys = Vec.fromList ([-6,-5..(-1)] ++ [2,4..20]) :: DVec
      aa = CCS cn rj cq ys :: CCSD
      -- And some sparse-vectors:
      ui = Vec.fromList [17,1,23,2,15,0,18,11,13,4] :: IVec
      ux = Vec.fromList [1..10] :: DVec
      uu = SparseVector 30 ui ux
      vi = Vec.fromList [29,6,14,7,23,9,21,22,18,0] :: IVec
      vx = Vec.fromList [5,7..23] :: DVec
      vv = SparseVector 30 vi vx
      nulbench = bgroup "Null" []

--   print =<< do
--     p <- malloc
--     poke p $ ccs2csD mm
--     peek p
--   print =<< csr2cs (csr'add (ccs2csrD mm) (ccs2csrD aa) 1 1)
  print $ cs'add (ccs2csD mm) (ccs2csD aa) 1 1

  defaultMain
    [ nulbench
    , matbench ee
--     , matbench zz
--     , matbench mm
    , addbench ee zz
--     , addbench aa mm
    , mulbench ee zz
--     , mulbench aa mm
    ]
